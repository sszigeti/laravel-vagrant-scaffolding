#!/bin/bash
## This file gets executed the first time you do a `vagrant up`.
## If you want it to run again you'll need run `vagrant provision`



MYSQL_ADMIN_PASSWORD="1234.passw0rd"



echo "Provisioning virtual machine..."

# Suppress the constant "dpkg-reconfigure: unable to re-open stdin: No file or directory" whining
export DEBIAN_FRONTEND=noninteractive

echo "Installing Vim, Midnight Commander, Ntp, Bzip2"
apt-get install vim mc ntp bzip2 -y > /dev/null

echo "Installing Git"
apt-get install git -y > /dev/null

echo "Setting up a nice bash prompt"
su -c 'ln -s /home/vagrant/synced/vagrant/etc/.bash_aliases /home/vagrant/.bash_aliases' - vagrant

echo "Installing PHP"
apt-get install php5-common php5-dev php5-cli php5-fpm -y > /dev/null

echo "Installing PHP extensions"
apt-get install curl php5-curl php5-gd php5-mcrypt php5-mysql -y > /dev/null

echo "Installing Nginx"
apt-get install nginx -y > /dev/null

echo "Configuring Nginx"
ln -s /home/vagrant/synced/vagrant/etc/nginx_vhost /etc/nginx/sites-enabled/
rm -rf /etc/nginx/sites-enabled/default
service nginx restart > /dev/null

echo "Preparing MySQL"
apt-get install debconf-utils -y > /dev/null
debconf-set-selections <<< "mysql-server mysql-server/root_password password $MYSQL_ADMIN_PASSWORD"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $MYSQL_ADMIN_PASSWORD"

echo "Installing MySQL"
apt-get install mysql-server -y > /dev/null

echo "Installing Composer globally"
curl -sS https://getcomposer.org/installer | php > /dev/null
mv composer.phar /usr/local/bin/composer

echo "Installing Laravel command line tool globally"
sudo -u vagrant -i composer global require "laravel/installer" > /dev/null
echo "PATH=\$PATH:~/.composer/vendor/bin" >> /home/vagrant/.bashrc
chown vagrant.vagrant /home/vagrant/.bashrc

echo "Finished provisioning."
